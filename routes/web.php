<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('login', 'AuthController@login' );
$router->post('register', 'AuthController@register' );


$router->group([ 'middleware' => 'auth' ], function() use ( $router ){

    $router->get('all', 'AuthController@getUser' );

    $router->post( 'bank-account', 'BankAccountController@store' );
    $router->get( 'bank-accounts[/{id}]', 'BankAccountController@get' );

    $router->post('account', 'AccountController@store' );
    $router->get('accounts[/{id}]', 'AccountController@get' );

    $router->post('deposit', 'DepositController@store' );
    $router->get('deposits[/{id}]', 'DepositController@get' );

    $router->post('order', 'OrderController@store' );
    $router->get('orders[/{id}]', 'OrderController@get' );

    $router->post('withdraw', 'WithdrawController@store' );
    $router->get('withdrawals[/{id}]', 'WithdrawController@get' );

    $router->get('volume', 'DataController@getVolume' );
    $router->get('history', 'DataController@getPriceHistory' );
    $router->get('balance', 'AccountController@getUserAccountBalance' );
    $router->get('performance', 'AccountController@getInvestmentPerformance' );

});
