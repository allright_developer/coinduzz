<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAccountTable extends Migration
{
    public $table = 'account';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'id_user' );
            $table->string( 'name', 30 );
            $table->timestamps();
            $table->unsignedTinyInteger( 'status' )->default( 1 );

            $table->unique( ['id_user', 'name' ], 'uq_account_user' );
            $table->foreign( 'id_user' )->references( 'id' )->on( 'user' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
