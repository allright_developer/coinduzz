<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTickerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticker', function (Blueprint $table) {
            $table->id();
            $table->string( 'coin' );
            $table->unsignedDecimal( 'last',16, 8 );
            $table->unsignedDecimal( 'buy',16, 8 );
            $table->unsignedDecimal( 'sell',16, 8 );
            $table->timestamp( 'at' )->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticker');
    }
}
