<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountPairTable extends Migration
{
    public $table = 'account_position';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'id_account' );
            $table->string('symbol', 10 )->default('BRL' );
            $table->decimal('amount', 14, 8)->default(0);
            $table->unique( [ 'symbol', 'id_account' ], 'uq_account_position' );
            $table->timestamps();

            $table->foreign( 'id_account', 'fk_position_account' )->references('id' )->on('account' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
