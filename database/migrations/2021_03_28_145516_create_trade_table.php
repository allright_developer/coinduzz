<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradeTable extends Migration
{
    public $table = 'order';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'id_user' );
            $table->unsignedBigInteger( 'id_account' );
            $table->unsignedTinyInteger('type' )->default(1 )->comment( '1=buy,0=sell' );
            $table->string('symbol', 5 )->default('BTC' );
            $table->decimal('amount', 14, 8 )->comment( 'amount to trade in the specified symbol' );
            $table->decimal( 'crypto_value_at_trade', 14, 8 )->comment( 'symbol value at the trading moment in BRL' );
            $table->dateTime( 'traded_at' )->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
