<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountTable extends Migration
{
    public $table = 'bank_account';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'id_user' );
            $table->unsignedBigInteger( 'id_bank' );
            $table->string('agency', 8 );
            $table->string('account', 12 );
            $table->timestamps();
            $table->unsignedTinyInteger( 'status' )->default(1);

            $table->foreign( 'id_user', 'fk_account_user' )->references('id')->on( 'user' );
            $table->foreign( 'id_bank', 'fk_account_bank' )->references('id')->on( 'bank' );

            $table->unique([ 'id_user', 'id_bank', 'agency', 'account' ], 'uq_user_bank_account' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
