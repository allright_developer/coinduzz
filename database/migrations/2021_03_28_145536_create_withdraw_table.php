<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawTable extends Migration
{
    public $table = 'withdraw';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user' );
            $table->unsignedBigInteger('id_account_position' );
            $table->unsignedBigInteger( 'id_bank_account' );
            $table->decimal( 'amount', 14, 6 )->comment( 'withdraw value' );
            $table->dateTime( 'withdrawn_at' )->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
