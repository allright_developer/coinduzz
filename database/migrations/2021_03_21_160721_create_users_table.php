<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public $table = 'user';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( $this->table, function (Blueprint $table) {
            $table->id();
            $table->string( 'name', 100 );
            $table->string( 'email' )->unique( 'uq_user' );
            $table->string( 'password' )
                ->nullable()
                ->comment( 'user password' );
            $table->timestamps();
            $table->unsignedTinyInteger('status' )->default(2)->comment('1=active,0=deactivated,2=not_verified' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( $this->table );
    }
}
