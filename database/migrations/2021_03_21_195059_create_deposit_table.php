<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositTable extends Migration
{
    public $table = 'deposit';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'id_user' );
            $table->unsignedBigInteger('id_account' );
            $table->unsignedBigInteger('id_bank_account' );
            $table->decimal('amount', 8, 2)->comment( 'amount to be credited in an user account' );
            $table->dateTime('provisioned_at')->useCurrent();

            $table->foreign('id_user', 'fk_deposit_user' )->references('id')->on( 'user' );
            $table->foreign('id_bank_account', 'fk_deposit_bank_account' )->references('id')->on( 'bank_account' );
            $table->foreign('id_account', 'fk_deposit_account' )->references('id')->on( 'account' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit');
    }
}
