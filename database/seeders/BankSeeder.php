<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            [
                'name' => 'Itaú', 'code' => 341
            ],
            [
                'name' => 'Bradesco', 'code' => 237
            ],
            [
                'name' => 'Nubank', 'code' => 260
            ]
        ];

        foreach ( $banks as $bank ){
            Bank::updateOrCreate( [ 'name' => $bank[ 'name' ] ], [ 'code' => $bank['code'] ] );
        }
    }
}
