<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MailableExample extends \Illuminate\Mail\Mailable
{
    use Queueable, SerializesModels;

    public string $content;

    public function __construct( string $content = 'Hello' )
    {
        $this->content = $content;
    }

    public function build()
    {
        return $this->view('mails.main' );
    }
}
