<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class DepositMail extends \Illuminate\Mail\Mailable
{
    use Queueable, SerializesModels;

    public string $userName;

    public string $depositValue;

    public function __construct( string $userName, string $depositValue )
    {
        $this->userName = $userName;
        $this->depositValue = $depositValue;
    }

    public function build()
    {
        return $this
            ->subject( 'Depósito Creditado em Conta' )
            ->view('mails.deposit' );
    }
}
