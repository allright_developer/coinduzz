<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class OrderBuyMail extends \Illuminate\Mail\Mailable
{
    use Queueable, SerializesModels;

    public array $data;

    public function __construct( array $data )
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->view('mails.order.buy' )
                ->with([
                    'userName' => $data[ 'user_name' ],
                    'symbol' => $data[ 'symbol' ],
                    'symbolAmount' => $data[ 'symbol_amount' ],
                    'fiatAmount' => $data[ 'fiat_amount' ]
                ]);
    }
}
