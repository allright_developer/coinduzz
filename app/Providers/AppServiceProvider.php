<?php

namespace App\Providers;

use App\Models\BankAccount;
use App\Models\Order;
use App\Rules\BankAccountRule;
use App\Services\Order\BuyOrderBehavior;
use App\Services\Order\OrderService;
use App\Services\Order\SellOrderBehavior;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind( BankAccountRule::class, function( $app ){
//            return new BankAccountRule( new BankAccount );
//        });
    }

    public function provides()
    {
    }
}
