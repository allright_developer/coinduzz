<?php

namespace App\Http\Controllers;

use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WithdrawController extends Controller
{
    public function get( $id = null )
    {
        $response = null;

        try {

            $withdrawals = Withdraw::query()->where( 'id_user', auth()->user()->id );
            if( $id ){
                $withdrawals->where('id', $id );
            }
            $withdrawals = $withdrawals->get( [ 'id_account_position', 'id_bank_account', 'amount', 'withdrawn_at' ] )->all();
            $response = response()->json( [ 'withdrawals' => $withdrawals ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function store(Request $request)
    {
        $response = null;

        try {
            $this->validate( $request, [
                'id_bank_account' => 'required|numeric',
                'id_account_position' => 'required|numeric',
                'amount' => 'required|numeric',
            ],
            [
                'amount.required' => 'Por favor, informe o valor a ser sacado',
                'amount.numeric' => 'O valor de saque informado não é válido',
                'id_account_position.required' => 'Posição a ser sacada não informada',
                'id_bank_account.required' => 'Por favor, informe para qual Conta Bancária deseja sacar',
            ]);

            $data = $request->post();
            $data[ 'id_user' ] = auth()->user()->id;
            $withdraw = (new Withdraw)->execute( $data );
            $response = response()->json( [ 'withdraw' => $withdraw->getAttributes( [ 'id', 'id_account_position', 'id_bank_account', 'amount', 'withdrawn_at' ] ) ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
