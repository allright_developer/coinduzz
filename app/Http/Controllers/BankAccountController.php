<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use App\Rules\BankAccountRule;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store( Request $request )
    {
        $response = null;

        try {
            $data = $request->post();
            $this->validate( $request, [
                'id_bank' => 'required|numeric',
                'agency' => 'required',
                'account' => 'required|string',
            ],
            [
                'id_bank.required' => 'Por favor, informe o Banco',
                'id_bank.numeric' => 'Por favor, informe um código de Banco',
                'agency.required' => 'Por favor, digite sua Conta Bancária',
                'account.required' => 'Por favor, informe o Número Conta Bancária',
            ]);
            /* @var BankAccountRule $bankValidator */
            $bankValidator = app(BankAccountRule::class );

            $data['id_user'] = auth()->user()->id;
            if( $bankValidator->validateUniqueAccount( $data ) ){
                /* @var BankAccount $newBank */
                $newBank = BankAccount::create( $data );
                $response = response()->json( [ 'id_bank' => $newBank->id ] );
            }
        } catch ( \Exception $e ) {
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function get( $id = null )
    {
        $response = null;

        try {

            $accounts = BankAccount::where( 'id_user', auth()->user()->id );
            if( $id ){
                $accounts->where( 'id', $id );
            }
            $accounts = $accounts->get( ['id', 'id_bank', 'agency', 'account' ] )->all();
            $response = response()->json( [ 'bank_accounts' => $accounts ] );

        } catch ( \Exception $e ) {
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
