<?php

namespace App\Http\Controllers;

use App\Mail\DepositMail;
use App\Mappers\SymbolMapper;
use App\Models\AccountPosition;
use App\Models\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class DepositController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get( $id = null )
    {
        $response = null;

        try {
            $deposits = Deposit::where( 'id_user', auth()->user()->id );
            if( $id ){
                $deposits->where( 'id', $id );
            }
            $deposits = $deposits->get( ['id', 'id_account', 'id_bank_account', 'amount', 'provisioned_at' ] )->all();
            $response = response()->json( [ 'deposits' => $deposits ] );

        } catch ( \Exception $e ) {
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function store( Request $request )
    {
        $response = null;

        try {
            $this->validate( $request, [
                'id_account' => 'required|numeric',
                'id_bank_account' => 'required|numeric',
                'amount' => 'required|numeric|min:5',
            ],
            [
                'id_account.required' => 'Por favor, informe sua Conta na Coinduzz para fazer o depósito',
                'id_bank_account.required' => 'Por favor, informe o banco de origem do valor a ser depositado',
                'amount.required' => 'Por favor, digite o valor de Depósito',
                'amount.numeric' => 'Por favor, digite o valor em Reais',
                'amount.min' => 'O valor mínimo a de Deposito é de :min Reais',
            ]);

            $data = $request->post();
            $user = auth()->user();
            $data[ 'id_user' ] = $user->id;
            $deposit = Deposit::create( $data );
            if( $deposit ){
                $accountPosition = AccountPosition::getByParams( [ 'id_account' => $deposit->id_account, 'symbol' => SymbolMapper::BRL ] );
                $accountPosition->increment( 'amount', (float) $data[ 'amount' ] );
                Mail::to( $user->email )
                    ->queue( new DepositMail( $user->name, 'R$ '.$data[ 'amount' ] ) );
                $response = response()->json( [ 'deposit' => $deposit->getAttributes(['id', 'id_account', 'id_bank_account', 'amount' ] ) ] );
            }

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
