<?php
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use  App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Validate and persist a new User to Coinduzz
     * @param  Request  $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $response = null;
        //validating incoming requests

        $this->validate($request, [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:user',
            'password' => 'required|string|min:6',
        ],
        [
            'name.required' => 'Digite o seu nome',
            'name.min' => 'Por favor, informe um nome válido (mínimo :min caracteres)',
            'email.required' => 'Por favor, informe o seu email',
            'email.unique' => 'Ops, este email já está sendo usado',
            'email.email' => 'Por favor, informe um endereço de email válido',
            'password.required' => 'Por favor, digite uma senha',
            'password.min' => 'Por favor, informe uma Senha com pelo menos :min Caracteres'
        ]);

        try {

            $user = new User;
            $user->name = $request->post('name');
            $user->email = $request->post('email');
            $passwd = $request->input('password');
            $user->password = app('hash')->make( $passwd );

            $saved = $user->save();

            if( $saved ){
                $credentials = $request->only( [ 'email', 'password' ] );
                $token = Auth::attempt( $credentials );
                $response = response()->json([ 'token' => $token, 'message' => 'created'], 201);
            }

        } catch (\Exception $e) {
            //return error message
            $response = response()->json(['message' => 'Houve um erro e não pudemos completar o registro' ], 409);
        }

        return $response;
    }

    /**
     * Get a token after a successful authentication
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login( Request $request )
    {
        $response = null;
        //validate incoming request
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
        ],
        [
            'email.required' => 'Por favor, informe o seu email',
            'email.email' => 'Por favor, informe um endereço de email válido',
            'password.required' => 'Por favor, digite uma senha',
            'password.min' => 'Por favor, informe uma Senha com pelo menos :min Caracteres'
        ]);

        $credentials = $request->only( [ 'email', 'password' ] );
        $token = Auth::attempt( $credentials );
        if ( !$token )
            $response = response()->json( ['message' => 'Unauthorized'], 401);
        else
            $response = $this->responseWithToken( $token );

        return $response;
    }

    public function refresh()
    {
        return $this->responseWithToken( \auth()->refresh() );
    }
    public function getUser( Request $request )
    {
        return response()->json([ 'users' => User::all(),'message' => 'ok' ] );
    }
}
