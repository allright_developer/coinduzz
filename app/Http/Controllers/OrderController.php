<?php

namespace App\Http\Controllers;

use App\Mail\OrderBuyMail;
use App\Mappers\OrderMapper;
use App\Mappers\SymbolMapper;
use App\Models\Order;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get( $id = null )
    {
        $response = null;

        try {
            $user = auth()->user;
            $orders = Order::query()
                ->where( 'id_user', $user->id );
            if( $id ){
                $orders->where( 'id', $id );
            }
            $orders = $orders->get( [ 'id', 'type', 'symbol', 'amount', 'crypto_value_at_trade', 'traded_at' ] )->all();
            $response = response()->json( [ 'orders' => $orders ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function store( Request $request )
    {
        $response = null;

        try {
            $this->validate( $request, [
                'id_account' => 'required',
                'type' => 'required',
                'symbol' => 'required',
                'amount' => 'required|numeric',
            ],
            [
                'id_account.required' => 'Por favor, informe a conta para executar a Ordem',
                'type.required' => 'o tipo de Ordem a ser executada não foi informada',
                'symbol.required' => 'Informe a moeda para executar a Ordem',
                'amount.required' => 'Por favor, informe o valor da Ordem',
                'amount.numeric' => 'Por favor, informe uma quantia válida',
            ]);
            /* @var OrderService $orderService */
            $orderService = app( OrderService::class );
            $result = $orderService->execute( $request->post() );
            if( $result ) {
                $attrs = $orderService->lastOrder->getAttributes();
                $user = auth()->user();
                $response = response()->json([ 'order' => $attrs ] );

                if( $attrs['type'] === OrderMapper::BUY ) {
                    $mailData = [
                        'user_name' => $user->name,
                        'symbol' => SymbolMapper::SYMBOLS[ $attrs[ 'symbol' ] ],
                        'symbol_amount' => $attrs[ 'crypto_value_at_trade' ],
                        'fiat_amount' => $attrs['amount'],
                    ];
                    Mail::to( $user->email )
                        ->queue( new OrderBuyMail( $mailData ) );
                }
            }

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
