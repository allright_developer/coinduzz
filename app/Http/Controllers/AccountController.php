<?php

namespace App\Http\Controllers;

use App\Mappers\OrderMapper;
use App\Mappers\SymbolMapper;
use App\Models\Account;
use App\Models\Order;
use App\Models\Ticker;
use App\Rules\AccountRule;
use App\Services\AccountService;
use App\Services\Order\OrderService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get( $id = null )
    {
        $response = null;

        try {

            $accounts = Account::query()->where( 'id_user', auth()->user()->id );
            if( $id ){
                $accounts->where( 'id', $id );
            }
            $accounts = $accounts->get( [ 'id', 'name', 'status' ] )->all();
            $response = response()->json( [ 'accounts' => $accounts ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function getUserAccountBalance()
    {
        $response = null;

        try {

            $accountBalances = app( AccountService::class )->getUserAccountBalance( auth()->user()->id );
            $response = response()->json( [ 'balances' => $accountBalances ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function getInvestmentPerformance( $idAccount = null )
    {
        $response = null;

        try {

            $lastTick = Ticker::query()->orderBy('id', 'desc')->first();
            /* @var Collection $orders */
            $orders = Order::where([
                            [ 'id_user', '=', auth()->user()->id ],
                            [ 'symbol', '=', SymbolMapper::BTC ],
                            [ 'type', '=', OrderMapper::BUY ]
                        ])->get( [ 'id', 'type', 'symbol', 'amount', 'amount_in_crypto', 'crypto_value_at_trade', 'traded_at', 'id_user' ] );

            $orders = $orders->map( function( $order ) use ( $lastTick ){

                $currentAmount = $lastTick->last * $order->amount_in_crypto;
                $variationInPercent = number_format( ( ( $currentAmount - $order->amount ) / $order->amount ) * 100, 2);

                $orderData = [
                    'amount_at_trade' => (float) $order->amount,
                    'amount_right_now' => $currentAmount,
                    'amount_in_crypto' => (float) $order->amount_in_crypto,
                    'variation_percent' => (float) $variationInPercent,
                    'price_at_trade' => (float) $order->crypto_value_at_trade,
                    'price_right_now' => (float) $lastTick->last,
                    'traded_at' => $order->traded_at,
                ];

                return $orderData;
            });
            $response = response( [ 'orders' => $orders, 'ticker' => $lastTick ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function store( Request $request )
    {
        $response = null;

        try {

            $this->validate( $request, [
                'name' => 'required',
            ],
            [
                'name.required' => 'Por favor, informe o nome da conta',
            ]);

            /* @var AccountRule $accontValidator */
            $accountValidator = app(AccountRule::class );
            $data = $request->post();
            $data['id_user'] = auth()->user()->id;

            if( $accountValidator->validate( $data ) ){
                $newAccount = Account::create( $data );
                $response = response()->json( [ 'account' => $newAccount->getAttributes([ 'id', 'name', 'status' ]) ] );
            }

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
