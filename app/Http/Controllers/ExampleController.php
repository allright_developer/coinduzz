<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(Request $request)
    {
        $response = null;

        try {


            $response = response()->json( [ ] );

        } catch ( \Exception $e ) {
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }

    public function store(Request $request)
    {
        $response = null;

        try {
            $this->validate( $request, [
                '' => 'required',
            ],
            [
                '.required' => 'Por favor, ',
            ]);

            $response = response()->json( [ ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
