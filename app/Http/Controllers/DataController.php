<?php

namespace App\Http\Controllers;

use App\Mappers\OrderMapper;
use App\Mappers\SymbolMapper;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Getting volume for the current day using Eloquent Model
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVolume()
    {
        $response = null;

        try {

            $buyVolume = Order::whereBetween( 'traded_at',
                    [
                        Carbon::now()->setTime(0, 0, 0)->format('Y-m-d H:i:s' ),
                        Carbon::now()->format('Y-m-d H:i:s' )
                    ]
                )
                ->where( 'symbol', SymbolMapper::BTC )
                ->where( 'type', OrderMapper::BUY )
                ->sum( 'amount_in_crypto' );

            $sellVolume = Order::whereBetween( 'traded_at',
                   [
                       Carbon::now()->setTime(0, 0, 0)->format('Y-m-d H:i:s' ),
                       Carbon::now()->format('Y-m-d H:i:s' )
                   ]
                )
                ->where( 'symbol', SymbolMapper::BTC )
                ->where( 'type', OrderMapper::SELL )
                ->sum( 'amount_in_crypto' );

            $response = response()->json( [ 'volume' => [ 'buy' => $buyVolume, 'sell' => $sellVolume ] ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);

        }

        return $response;
    }

    /**
     * Example of how to use native PHP datetime handling methods and Laravel DB Facades
     * @param string $period same as used with PHP DateInterval class
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPriceHistory( $period = 'PT24H' )
    {
        $response = null;

        try {

            $from = (new \DateTime())->sub( new \DateInterval( $period ) )->format('Y-m-d H:i:s' );
            $to = (new \DateTime() )->format('Y-m-d H:i:s' );

            $result = DB::select(
                "SELECT
                            buy, sell, at
                        FROM
                            ticker
                        WHERE
                            at BETWEEN :from AND :to and coin = :coin",
                [ 'from' => $from, 'to' => $to, 'coin' => SymbolMapper::BTC ]
            );

            $response = response()->json( [ 'history' => $result ] );

        } catch ( \Exception $e ) {
            Log::error( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ] );
            $response = response()->json([ 'error' =>  $e->getMessage() ], 409);
        }

        return $response;
    }
}
