<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'account';
    /**
     * @var array
     */
    protected $guarded = [ 'id' ];
}
