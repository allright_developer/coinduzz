<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    /**
     * @var array
     */
    protected $guarded = [ 'id' ];
}
