<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountPosition extends Model
{
    protected $table = 'account_position';
    /**
     * @var array
     */
    protected $guarded = [ 'id' ];

    /**
     * @param array $params
     * @return AccountPosition
     */
    public static function getByParams( array $params )
    {
        $accountPosition = null;

        if( isset( $params['id'] ) )
            $accountPosition = self::find( $id );
        else{

            $where = [];
            foreach ($params as $field => $value ){
                $where[ $field ] = $value;
            }
            $accountPosition = self::firstOrCreate( $where );
        }

        return $accountPosition;
    }

    public function getAmountAttribute( $value )
    {
        return (float) $value;
    }
}
