<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'withdraw';
    /**
     * @var array
     */
    protected $guarded = [ 'id' ];

    public $timestamps = false;

    /**
     * @param array $params
     * @return Withdraw
     * @throws \Exception
     */
    public function execute( array $params ): Withdraw
    {
        $accountPosition = $this->getUserAccountPosition( $params );
        if( !$accountPosition || $accountPosition->amount < $params[ 'amount' ] )
            throw new \Exception( 'Não há fundos suficientes para realizar o saque' );

        $result = Withdraw::create( $params );

        if( $result )
            $accountPosition->decrement( 'amount', $params[ 'amount' ] );
        else
            throw new \Exception( 'Não foi possível realizar o saque' );

        return $result;
    }

    public function getUserAccountPosition( array $params )
    {
        $accountPosition = AccountPosition::find( $params[ 'id_account_position' ] );

        return $accountPosition;
    }
}
