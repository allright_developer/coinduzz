<?php
namespace App\Services;

use App\Mappers\OrderMapper;
use App\Mappers\SymbolMapper;
use App\Models\Account;
use App\Models\AccountPosition;
use App\Services\Ticker\TickerService;
use Illuminate\Support\Facades\DB;

class AccountService extends AbstractService
{
    public $repository;

    public function __construct( Account $repository )
    {
        $this->repository = $repository;
    }

    public function execute( array $options ): bool
    {
        return true;
    }

    public function getAccountPositions( int $id )
    {
        $positions = [];
        foreach( SymbolMapper::SYMBOLS as $symbol => $name ){

            $accountPosition = AccountPosition::
                  where('id_account', $id )
                ->where( 'symbol', $symbol )->first();

            $positions[ $symbol ] = $accountPosition ? $accountPosition->amount : 0;
        }

        return $positions;
    }

    /**
     * Get user account balances
     * @param int $idUser
     * @return \Illuminate\Support\Collection
     */
    public function getUserAccountBalance( int $idUser )
    {
        $balances =
            DB::select("
            SELECT
                a.id id_account, a.name as account_name,
                ap.amount, ap.symbol
            FROM
                account a
            INNER JOIN
                account_position ap on a.id = ap.id_account
            WHERE
                a.id_user = :id_user
        ", [ 'id_user' => $idUser ]);

        $balances = collect( $balances )->groupBy( 'account_name' );

        return $balances;
    }

    public function getAccountException( array $data )
    {
        return new \Exception( $data['message'] );
    }
}
