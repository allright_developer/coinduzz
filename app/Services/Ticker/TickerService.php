<?php

namespace App\Services\Ticker;


use App\Models\Ticker;
use App\Services\AbstractService;
use App\Services\Ticker\iTicker;

class TickerService extends AbstractService implements iTicker
{
    public $repository;

    public function __construct( Ticker $repository )
    {
        $this->repository = $repository;
    }

    public function execute( array $options ): bool
    {
        $data = $this->getLastTick( $options );
        return $this->persist( $data );
    }

    public function getLastTick( array $options ): array
    {
        $data = [];
        $curlResource = $this->getTickerStreamResource( $options['symbol'] );
        $result = \curl_exec( $curlResource );
        $hasErrors = \curl_error( $curlResource );
        if( !$hasErrors ){
            $result = \json_decode( $result );
            $data = [
                'last' => (float) $result->ticker->last,
                'buy' => (float) $result->ticker->buy,
                'sell' => (float) $result->ticker->sell,
                'coin' => $options[ 'symbol' ],
                'at' => \date( 'Y-m-d H:i:s' )
            ];
        }
        else{
            throw new \Exception( "Error on ticker" );
        }

        return $data;
    }
    /**
     * persists a new tick data
     * @param array $data
     * @return bool
     */
    public function persist( array $data ): bool
    {
        $result = false;

        if( $data ){
            $ticker = $this->repository->fill( $data );
            $result = $ticker->save();
        }

        return $result;
    }
    /**
     * Initiate a new curl resource to fetch ticker data
     * Although GuzzleHttp is largely used nowadays, I prefer employ pure php when it is possible to improve performance
     * @param string $symbol
     * @return mixed
     */
    public function getTickerStreamResource( string $symbol = 'BTC' )
    {
        $endpoint = preg_replace_array('/\{symbol\}/', [$symbol], config('ticker.endpoint' ) );
        $curlResource = \curl_init( $endpoint );
        \curl_setopt( $curlResource,\CURLOPT_RETURNTRANSFER, true );

        return $curlResource;
    }
}
