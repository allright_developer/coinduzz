<?php

namespace App\Services\Order;

interface iOrderBehavior
{
    /**
     * Execute an Order operation
     * @param array $orderData
     * @return array
     */
    function execute( array $orderData ): array;
}
