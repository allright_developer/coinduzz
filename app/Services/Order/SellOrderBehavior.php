<?php

namespace App\Services\Order;


use App\Mappers\SymbolMapper;
use App\Models\AccountPosition;

class SellOrderBehavior extends AbstractOrderAction implements iOrderBehavior
{

    public function execute( array $orderData ): array
    {
        $accountPositionToSell = AccountPosition::getByParams( [ 'id_account' => $orderData['id_account'], 'symbol' => $orderData[ 'symbol'] ] );
        if( $accountPositionToSell['amount'] < $orderData['amount'] ){
            throw $this->getOrderException( [ 'symbol' => $orderData['symbol'] ] );
        }

        $accountPositionBRL = AccountPosition::getByParams( [ 'symbol' => SymbolMapper::BRL, 'id_account' => $orderData[ 'id_account' ] ] );
        $orderData[ 'amount_in_crypto' ] = $orderData[ 'amount' ];
        $orderData['amount'] = $orderData[ 'crypto_value_at_trade' ] * $orderData[ 'amount' ];
        $accountPositionToSell->decrement( 'amount', $orderData['amount_in_crypto'] );
        $accountPositionBRL->increment( 'amount', $orderData[ 'amount' ] );

        return $orderData;
    }
}
