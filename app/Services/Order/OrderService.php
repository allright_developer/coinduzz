<?php
namespace App\Services\Order;

use App\Mappers\OrderMapper;
use App\Mappers\SymbolMapper;
use App\Models\Order;
use App\Services\AbstractService;
use App\Services\Ticker\TickerService;

class OrderService extends AbstractService
{
    public $repository;
    /**
     * @var Order
     */
    public $lastOrder = null;
    /**
     * last market value for a symbol at the momento of an Order execution
     * @var float
     */
    public $lastTick = 0;

    /**
     * @var iOrderBehavior|BuyOrderBehavior
     */
    public iOrderBehavior $buy;
    /**
     * @var iOrderBehavior|SellOrderBehavior
     */
    public iOrderBehavior $sell;

    public function __construct( Order $repository, BuyOrderBehavior $buy, SellOrderBehavior $sell )
    {
        $this->repository = $repository;
        $this->buy = $buy;
        $this->sell = $sell;
    }

    public function execute( array $options ): bool
    {
        $options[ 'crypto_value_at_trade' ] = $this->getLastTick();
        switch ( $options['type'] ){
            case OrderMapper::BUY :
                $options = $this->buy( $options );
                break;
            case OrderMapper::SELL:
                $options = $this->sell( $options );
                break;
            default:
                throw new \Exception( 'Tipo de Ordem Indefinido' );
        }

        $this->lastOrder = Order::create([
            'id_user' => auth()->user()->id,
            'id_account' => $options[ 'id_account' ],
            'type' => $options[ 'type' ],
            'symbol' => $options[ 'symbol' ],
            'amount' => $options[ 'amount' ],
            'amount_in_crypto' => $options[ 'amount_in_crypto' ],
            'crypto_value_at_trade' => $options[ 'crypto_value_at_trade' ]
        ]);

        return $this->lastOrder ? true : false;
    }

    /**
     * Make the swap from fiat to crypto
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function buy( array $options )
    {
        return $this->buy->execute( $options );
    }

    /**
     * Make the swap from crypto to fiat
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function sell( array $options )
    {
        return $this->sell->execute( $options );
    }

    public function getLastTick( $symbol = SymbolMapper::BTC, $ticker = TickerService::class )
    {
        if( $this->lastTick === 0 )
            $this->lastTick = app( $ticker )->getLastTick( [ 'symbol' => $symbol ] )['last'];

        return $this->lastTick;
    }
}
