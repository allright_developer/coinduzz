<?php

namespace App\Services\Order;

use App\Mappers\SymbolMapper;
use App\Models\AccountPosition;

class BuyOrderBehavior extends AbstractOrderAction implements iOrderBehavior
{
    public function execute( array $orderData ): array
    {
        $accountPositionBRL = AccountPosition::getByParams( [ 'id_account' => $orderData['id_account'], 'symbol' => SymbolMapper::BRL ] );
        if( $accountPositionBRL[ 'amount' ] < $orderData[ 'amount' ] ) {
            throw $this->getOrderException( [ 'symbol' => $orderData['symbol'] ] );
        }

        $orderData[ 'amount_in_crypto' ] = number_format( $orderData['amount'] / $orderData[ 'crypto_value_at_trade' ], 8 );
        $accountPositionBRL->decrement('amount', $orderData[ 'amount' ] );

        $accountPositionSymbol = AccountPosition::getByParams([ 'symbol' => $orderData['symbol'], 'id_account' => $orderData[ 'id_account' ] ]);
        $accountPositionSymbol->increment( 'amount', $orderData[ 'amount_in_crypto' ] );

        return $orderData;
    }
}
