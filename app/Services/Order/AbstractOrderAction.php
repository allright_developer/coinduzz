<?php

namespace App\Services\Order;

use App\Mappers\SymbolMapper;
use Illuminate\Support\Facades\Log;

abstract class AbstractOrderAction
{
    /**
     * Launch an Exception when Order operations fails
     * @param array $data
     * @return \Exception
     */
    public function getOrderException( array $data )
    {
        $message = preg_replace('/\{symbol\}/', SymbolMapper::SYMBOLS[ $data['symbol'] ],
            "Não há fundos suficientes em {symbol} para executar a Ordem"
        );
        Log::debug( $message, [ 'class' => \get_class( $this ) ] );

        return new \Exception( $message );
    }
}
