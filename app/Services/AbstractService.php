<?php

namespace App\Services;

abstract class AbstractService
{
    protected abstract function execute( array $options ) : bool;
}
