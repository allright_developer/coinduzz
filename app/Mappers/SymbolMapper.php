<?php

namespace App\Mappers;


class SymbolMapper
{
    const BRL = 'BRL';
    const BTC = 'BTC';

    const SYMBOLS = [
        'BRL' => 'Real',
        'BTC' => 'Bitcoin',
    ];
}
