<?php
namespace App\Console\Commands;

use App\Models\Ticker;
use App\Services\AbstractService;
use App\Services\Ticker\TickerService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class TickerCommand extends Command
{
    protected $signature = 'coinduzz:ticker {symbol=BTC : coin symbol}';

    protected $description = 'get a ticker and persists to ticker table';

    public function handle()
    {
        $this->getTicker();
    }

    /**
     * Get a new ticker position
     * @return $this
     */
    protected function getTicker()
    {
        try {
            $service = App::make( TickerService::class );
            $service->execute( [ 'symbol' => $this->argument('symbol' ) ] );
            $this->comment('new ticker created succesfully' );
        }
        catch( \Exception $e ){
            $this->error('Erro na recuperação, analise os logs da aplicação para mais informações' );
            Log::debug( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ]);
        }
        return $this;
    }
}
