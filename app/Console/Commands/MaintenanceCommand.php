<?php
namespace App\Console\Commands;

use App\Models\Ticker;
use Illuminate\Console\Command;

class MaintenanceCommand extends Command
{
    protected $signature = 'coinduzz:maintenance';

    protected $description = 'get a ticker and persists to ticker table';

    public function handle()
    {
        $this->purgeOldTickers();
    }

    /**
     * Remove ticker positions older than 90 days
     * @return $this
     */
    protected function purgeOldTickers()
    {
        Ticker::where('at', '<', (new \DateTime())->sub( new \DateInterval('P90D' ) )->format( 'Y-m-d H:i:s' ) )->delete();
        return $this;
    }
}
