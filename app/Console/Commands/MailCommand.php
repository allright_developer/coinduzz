<?php
namespace App\Console\Commands;

use App\Mail\MailableExample;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailCommand extends Command
{
    protected $signature = 'coinduzz:mail {to : recipient email address}';

    protected $description = 'send a test email';

    public function handle()
    {
        try{
            Mail::to( $this->argument('to') )
//                ->send( new MailableExample( 'Hello testing') );
                ->queue( new MailableExample( 'Hello testing') );
        }
        catch ( \Exception $e ){
            $this->error('Erro no envio de Email, analise os logs da aplicação para mais informações' );
            Log::debug( $e->getMessage(), [ 'file' => $e->getFile(), 'line' => $e->getLine() ]);
        }

    }
}
