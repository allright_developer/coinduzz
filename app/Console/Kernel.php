<?php

namespace App\Console;

use App\Console\Commands\MailCommand;
use App\Console\Commands\MaintenanceCommand;
use App\Console\Commands\TickerCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * @var array
     */
    protected $commands = [
        TickerCommand::class, MailCommand::class, MaintenanceCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('coinduzz:ticker' )->everyFiveMinutes();
        $schedule->command('coinduzz:maintenance' )->daily();
    }
}
