<?php
namespace App\Rules;

use App\Models\Account;

class AccountRule
{
    public $repository;

    public function __construct( Account $repository )
    {
        $this->repository = $repository;
    }

    public function validate( array $fields ): bool
    {
        return $this->validateUniqueAccount( $fields );
    }

    /**
     * Allow only one account with the same bank, agency and account data
     * @param array $fields
     * @return bool
     * @throws \Exception
     */
    public function validateUniqueAccount( array $fields ): bool
    {
        $where = [];
        foreach ( $fields as $field => $value ){
            $where[] = [ $field, '=', $value ];
        }

        $result = $this->repository->newQuery()->where( $where )->first();

        return $result ? throw new \Exception( 'O nome escolhido já está sendo usado por esta conta' ) : true;
    }
}
